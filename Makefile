tinydyndns: tinydyndns-update tinydyndns-data

tinydyndns-update: load tinydyndns-update.o libtai.a env.a cdb.a dns.a \
alloc.a buffer.a unix.a byte.a
	./load tinydyndns-update strerr_sys.o strerr_die.o libtai.a env.a \
	cdb.a dns.a alloc.a buffer.a unix.a byte.a

tinydyndns-update.o: compile tinydyndns-update.c str.h strerr.h cdb.h \
dns.h iopause.h uint64.h uint32.h
	./compile tinydyndns-update.c

tinydyndns-data: load tinydyndns-data.o libtai.a env.a cdb.a dns.a alloc.a \
buffer.a unix.a byte.a
	./load tinydyndns-data strerr_sys.o strerr_die.o libtai.a env.a \
	cdb.a dns.a alloc.a buffer.a unix.a byte.a

tinydyndns-data.o: compile tinydyndns-data.c uint32.h uint64.h iopause.h
	./compile tinydyndns-data.c

clean:
	rm -f `cat TARGETS` \
	tinydyndns-update tinydyndns-update.o \
	tinydyndns-data tinydyndns-data.o

install-tinydyndns:
	install -m0755 tinydyndns-conf /usr/local/bin/tinydyndns-conf
	install -m0755 tinydyndns-data /usr/local/bin/tinydyndns-data
	install -m0755 tinydyndns-update /usr/local/bin/tinydyndns-update
	
include ./Makefile.djbdns
